//
//  DetailViewController.swift
//  Project1
//
//  Created by Роман Хоменко on 27.03.2022.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    var selectedImage: String?
    var selectedImageIndex: String?
    var numberOfImages: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Picture \(selectedImageIndex ?? "") of \(numberOfImages ?? "")"
        navigationItem.largeTitleDisplayMode = .never

        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(shareTapped))
        
        if let imageToLoad = selectedImage {
            imageView.image = UIImage(named: imageToLoad)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        navigationController?.hidesBarsOnTap = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        navigationController?.hidesBarsOnTap = false
    }
    
    @objc func shareTapped() {
        guard let image = imageView.image else {
            print("no image found")
            return
        }
        
        let preparedImageWithText = addTextToImage(image: image)
        preparedImageWithText.jpegData(compressionQuality: 0.8)
        
        let newImage = addTextToImage(image: image)
        
        let activityVC = UIActivityViewController(activityItems: [newImage], applicationActivities: [])
        activityVC.popoverPresentationController?.barButtonItem = navigationItem.rightBarButtonItem
        present(activityVC, animated: true)
    }
}

extension DetailViewController {
    func addTextToImage(image: UIImage) -> UIImage {
        let render = UIGraphicsImageRenderer(size: CGSize(width: image.size.width,
                                                          height: image.size.height))
        
        let newImage = render.image { ctx in
            image.draw(at: CGPoint(x: 0,
                                   y: 0))
            
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.alignment = .center
            
            let attrs: [NSAttributedString.Key: Any] = [
                .font: UIFont.systemFont(ofSize: 18),
                .paragraphStyle: paragraphStyle
            ]
            
            let string = "From Storm Viewer"
            
            let attributedString = NSAttributedString(string: string,
                                                      attributes: attrs)
            
            attributedString.draw(with: CGRect(x: image.size.width / 5,
                                               y: image.size.height / 10,
                                               width: image.size.width / 2,
                                               height: image.size.height / 10),
                                  options: .usesLineFragmentOrigin,
                                  context: nil)
        }
        
        return newImage
    }
}
